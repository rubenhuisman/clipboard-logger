# README #

Clipboard-Log is a powerful clipboard manager, which records every piece of data that goes to the Windows clipboard,
meaning that you can easily retrieve any information that was once copied to the clipboard. 
Click on a clipboard-item to get your data back on the clipboard!

Download http://apps.rubenhuisman.nl/clipboard-log

Version: 1.0