﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clipboard_Log
{
    public partial class Form1 : Form
    {
        List<String> clipboardItems = new List<String>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.AllowUserToAddRows = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            String getClipboard = Clipboard.GetText();
            String currentTime = DateTime.Now.ToString();

            if (!clipboardItems.Contains(getClipboard) && getClipboard != "")
            {
                this.dataGridView1.Rows.Add(currentTime, getClipboard);
                clipboardItems.Add(getClipboard);
                this.dataGridView1.Sort(this.dataGridView1.Columns["date"], ListSortDirection.Descending);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            clipboardItems.Clear();
            Clipboard.Clear();
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
            Clipboard.SetText(cell.Value.ToString());
        }
    }
}
